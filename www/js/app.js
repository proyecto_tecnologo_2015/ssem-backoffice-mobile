var app = angular.module('ssemMobile', ['ionic', 'ionic-material',
    //Custom propio
    'AppInterceptors',
    'ionicSelect',
    //Libs
    'dateParser',
    'angular-carousel',
    'ngDraggable'
]);

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});

app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom');

    $stateProvider

        .state('login', {
            url: '/login',
            templateUrl: 'templates/user/login.html',
            controller: 'LoginController'
        })
        .state('main', {
            url: '/main',
            templateUrl: 'templates/home/main.html',
            controller: 'MainController'

        })
        .state('registrar', {
            url: '/registrar',
            templateUrl: 'templates/user/registrar.html',
            controller: 'RegistrarController'
        })
        .state('altaEvento', {
            url:'/altaEvento',
            templateUrl: 'templates/home/evento/alta_evento.html',
            controller: 'EventoController'

        })
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        .state('app.home', {
            url: '/home',
            views : {
                'menuContent' : {
                    templateUrl : 'templates/home/home.html',
                    controller: 'HomeController'
                }
            }
        })
        .state('app.novedades', {
            url: '/novedades',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/user/novedad/novedad.html',
                    controller: 'NovedadesController'
                }
            }
        })
        .state('app.novedades.detalleNovedad', {
            url:'/detalle',
            views: {
                'menuContent': {
                    templateUrl: 'templates/user/novedad/detalle_novedad.html'
                }
            }
        })
        .state('app.encuentros', {
            url: '/encuentros',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/user/encuentro/lista_encuentro.html',
                    controller: 'EncuentroController'
                }
            }
        })
        .state('app.encuentros.detalleEncuentro', {
            url: '/detalle',
            views: {
                'menuContent@app' : {
                    templateUrl: 'templates/user/encuentro/detalle_encuentro.html',
                    controller: 'DetalleEncuentroController'
                }
            }
        })
        .state('app.notificar', {
            url: '/notificar',
            views: {
                'menuContent' : {
                    templateUrl: 'templates/user/notificacion/notificar.html',
                    controller: 'NotificacionController'
                }
            }
        })
        .state('app.encuentros.altaResultado', {
            url: '/resultados',
            views: {
                'menuContent@app' : {
                    templateUrl: 'templates/user/encuentro/alta_resultado.html',
                    controller: 'ResultadoController'
                }
            }
        });


    $urlRouterProvider.otherwise('login')

});

app.run(['$rootScope', function ($rootScope) {

    $rootScope.dateFormat = "dd/MM/yyyy";
    $rootScope.dateTimeFormat = "dd/MM/yyyy HH:MM";

    var ssl = true;

    $rootScope.SERVER_URL = (ssl ? 'https' : 'http') + '://services.ssem.org:8080/';

    $rootScope.initBackendUrls = function(SERVER_URL) {
        $rootScope.SSEM_BACKEND_REST_DEF = 'ssem-backend/services/rest/';
        $rootScope.SSEM_BACKEND_COMMON = SERVER_URL + 'ssem-backend/services/rest/common/';
        $rootScope.SSEM_BACKEND_ENDPOINT = SERVER_URL + 'ssem-backend/services/rest/backoffice/';
        $rootScope.SSEM_BACKEND_SECURITY_ENDPOINT = SERVER_URL + 'ssem-backend/services/rest/security/';
        $rootScope.SSEM_BACKEND_ANDROID = SERVER_URL + 'ssem-backend/services/rest/android/';
    };

    $rootScope.initBackendUrls($rootScope.SERVER_URL);


}]);


