/**
 * Created by dalepo on 04/05/16.
 */
app.service('notificacionService', function($q, $http, $rootScope){

    this.pushNotificacionEvento = function(notification, idEvento) {

        var defer = $q.defer();
        var request = {
            notification: notification,
            idEvento: idEvento
        };

        var url = $rootScope.SSEM_BACKEND_ANDROID + 'notifications/pushNotificationEvento ';
        $http.post(url, {request : request})
            .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

});