app.factory('validationUtils', function(){

    var validateLengthArray = function (array) {
        if(array && array.length > 0 ){
            return true;
        }
        return false;
    };

    return {
        validateLengthArray : validateLengthArray
    };

});