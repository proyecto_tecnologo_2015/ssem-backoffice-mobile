'use strict';
app.service('organizadorService', ['$rootScope', '$http', '$q',
    function($rootScope, $http, $q) {

        this.registrarOrganizador = function(request) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_SECURITY_ENDPOINT + 'operador/registro';

            $http.post(url, angular.copy(request))
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getEventosOrganizador = function(idOrganizador) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/getEventosOrganizador';
            $http.get(url)
                .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getDelegaciones = function(idEvento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON + "mostrar/verDelegacionesEvento";
            $http.get(url, {params: {idEvento: idEvento}})
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.crearDelegacion = function(request) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'operadores/altaDelegacion';
            $http.post(url, request)
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.editarOperador = function(request) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'operadores/editarOperador';
            $http.post(url, request)
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        }

    }]);