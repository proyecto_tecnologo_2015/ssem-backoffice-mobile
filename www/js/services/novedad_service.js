'use strict';
app.service('novedadService', ['$rootScope', '$http', '$q',
    function ($rootScope, $http, $q) {

        this.altaNovedad = function (novedad) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'novedades/altaNovedad';
            $http.post(url, novedad)
                .then(
                function success(response) {
                    defer.resolve(response);
                },
                function error(error) {
                    defer.reject(error);
                }
            );
            return defer.promise;
        };
        this.getNovedadesEvento = function (idEvento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verNovedadesEvento';
            $http.get(url, {params: {idEvento: idEvento}})
                .then(
                function success(response) {
                    defer.resolve(response.data);
                },
                function error(error) {
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.bajaNovedad = function(novedad) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'novedades/bajaNovedad';
            $http.post(url, novedad)
                .then(
                    function success(response) {
                        defer.resolve(response.data.response);
                    },
                    function error(error) {
                        defer.reject(error);
                    }
                );
            return defer.promise;
        };

        return this;
    }]);
