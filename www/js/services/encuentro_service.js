app.service('encuentroService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    this.getEncuentrosEvento = function(idEvento) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/getEncuentroEvento';
        $http.get(url, {params: {idEvento: idEvento}}).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getDetalleEncuentro = function(idEncuentro) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verEncuentro';
        $http.get(url, {params: {idEncuentro: idEncuentro}}).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.altaResultado = function(resultado) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'encuentro/altaResultado';
        $http.post(url, resultado).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;

    };

}]);
