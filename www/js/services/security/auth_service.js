app.service('authService', ['$localStorage', '$state', '$rootScope', function($localStorage, $state, $rootScope, $ionicHistory){

    var USER_STORE_LOCATION = 'SSEM_LOGGED_USER';
    var USER_HEADER_STORE_LOCATION = 'SSEM_USER_HEADER';

    var _PERFIL_MOD = 'MODERADOR';
    var _PERFIL_ORG = 'ORGANIZADOR';
    var _PERFIL_DEL = 'DELEGACION';

    var storeUser = function(user) {
        setUser(user);
    };

    var setUser = function(user) {
        $localStorage.putObject(USER_STORE_LOCATION, user);
    };

    var authorize = function(perfil) {
        var user = getUser();
        if(user) {
            return user.perfil.nombre === perfil;
        }
        return false;
    };


    var getUser = function() {
        return $localStorage.getObject(USER_STORE_LOCATION);
    };

    var userIsModerador = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_MOD;
    };

    var userIsDelegacion = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_DEL;
    };

    var userIsOrganizador = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_ORG;
    };

    var isAuthenticated = function() {
        var user = getUser();
        return user !== null && typeof user  == 'object';
    };

    var logout = function() {
        $localStorage.remove(USER_STORE_LOCATION);
        $localStorage.remove(USER_HEADER_STORE_LOCATION);
    };

    var getUserId = function() {
        var user = getUser();
        return user.id;
    };

    var redirectUserStart = function() {
        //Si no esta haciendo ninguna transicion y esta autenticado..
        if(isAuthenticated() && $state.transition == null) {
            if(userIsOrganizador()) {
                $state.go('app.home');
            }
            else {
                logout();
            }
        }
    };
    /**
     * Construye el header en base a los datos del cookie.
     * Lo parsea a json y luego b64
     * @returns {*|string}
     */
    var getHeader = function() {
        return $localStorage.get(USER_HEADER_STORE_LOCATION);
    };

    var storeUserHeader = function(headerStr) {
        $localStorage.put(USER_HEADER_STORE_LOCATION, headerStr);
    };

    return {
        userIsOrganizador: userIsOrganizador,
        isAuthenticated: isAuthenticated,
        getUser: getUser,
        storeUser: storeUser,
        authorize: authorize,
        logout: logout,
        redirectUserStart: redirectUserStart,
        getHeader: getHeader,
        getUserId: getUserId,
        storeUserHeader: storeUserHeader
    }

}]);
