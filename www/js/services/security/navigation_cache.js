app.factory('navigationCache', ['$localStorage', function($localStorage){

    var _selected_evento = 'SSEM_SELECTED_EVENTO';
    var _evento_delegacion = 'SSEM_EVENTO_DELEGACION';
    var _selected_encuentro = 'SSEM_SELECTED_ENCUENTRO';
    var _cacheindexes = [];

    _cacheindexes.push(_selected_evento);

    var cleanCache = function() {
        _cacheindexes.forEach(function(val) {
            $localStorage.remove(val);
        });
    };

    var memoryCache = function() {

        var self = {};
        self.map = {};

        self.put = function(key, object) {
            self.map[key] = object;
        };
        self.get = function(key) {
            return self.map[key];
        };
        self.remove = function(key) {
            delete self.map[key];
        };
        self.removeAll = function(){
            self.map = {};
        };
        return self;
    };


    var setSelectedEvento = function(evento) {
        $localStorage.putObject(_selected_evento, evento);
    };

    var getSelectedEvento = function() {
        return $localStorage.getObject(_selected_evento);
    };

    var getEventoDelegacion = function() {
        return $localStorage.getObject(_evento_delegacion);
    };

    var setSelectedEncuentro = function(encuentro) {
        return $localStorage.putObject(_selected_encuentro, encuentro);
    };

    var getSelectedEncuentro = function() {
        return $localStorage.getObject(_selected_encuentro);
    };

    var setEventoDelegacion = function(evento) {
        delete evento.imagenes;
        $localStorage.putObject(_evento_delegacion, evento);
    };

    return {
        cleanCache: cleanCache,
        setSelectedEvento: setSelectedEvento,
        getSelectedEvento: getSelectedEvento,
        getEventoDelegacion: getEventoDelegacion,
        setEventoDelegacion: setEventoDelegacion,
        setSelectedEncuentro : setSelectedEncuentro,
        getSelectedEncuentro : getSelectedEncuentro,
        memoryCache: memoryCache()

    }
}]);