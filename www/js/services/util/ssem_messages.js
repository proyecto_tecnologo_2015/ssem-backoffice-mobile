app.factory('ssemMessages', function() {
    var instance =  { };

    instance.GENERIC_ERROR = 'Ocurrió un error inesperado. Intente más tarde.';
    instance.ERROR = 'Error';
    instance.SUCCESS = 'Operación con éxito';
    instance.MSG_SUCCESS_REGISTRO = 'Se registró el operador exitosamente.';
    instance.MSG_SUCCESS_ALTA_EVENTO = 'Se dió de alta el evento correctamente.';
    instance.MSG_SUCCESS_ALTA_RESULTADO = 'Se cargarons los resultados correctamente.';
    instance.MSG_SUCCESS_NOTIFICACION = 'Se envió la notificación correctamente';
    instance.VALIDATE_MSG_ALTA_EVENTO_DISCIPLINAS = 'Debe agregar por lo menos una disciplina.';
    instance.VALIDATE_MSG_ALTA_EVENTO_IMAGENES= 'Debe agregar por lo menos una imágen.';
    instance.VALIDATE_NOTIFICACION_TITULO = 'Debe ingresar un título.';
    instance.VALIDATE_NOTIFICACION_BODY = 'Debe ingresar un contenido.';
    return instance;
});