app.factory('commonUtils', [ '$rootScope', function($rootScope){

    var converDateToArray = function(date) {
        return [
            date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes()
        ];
    };

    //ctm javascript
    var parseDateFromString = function(strDate) {
        strDate = strDate.replace(/-/, '/')
            .replace(/-/, '/');

        return new Date(strDate);

    };

    var buildUrlEvento = function(evento) {
        return 'https://104.197.119.166/web/#/home/verEvento?IdEvento='+evento.idEvento;
    };

    //Chanchisimo, pero el formato que mandan en el backend es un asco. Se deberia usar el formato ISO estandar.
    var parseDateFromArray = function(array) {
        if(!array.length || array.length == 0)
            return null;
        var date = new Date();

        date.setFullYear(array[0]);
        date.setMonth(array[1]-1);
        date.setDate(array[2]);

        if(array.length > 4) {
            date.setHours(array[3]);
            date.setMinutes(array[4]);
        }
        return date;
    };

    var Arrays = {

        hasElement: function (array, element) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] === element) {
                    return true;
                }
            }
            return false;
        },
        isEmpty : function(array) {
            if(angular.isArray(array))
                return array.length  == 0;
            return true;
        },
        isNotEmpty: function(array) {
            return !this.isEmpty(array);
        }

};


    var isUrlForBackend = function(url) {
        return angular.isString(url) && url.indexOf($rootScope.SSEM_BACKEND_REST_DEF) !== -1;
    };
    var isNotUrlForLogin = function(url){
        return url.indexOf('loginFirstStep') === -1 && url.indexOf('tryLogin') === -1;
    };
    return {
        converDateToArray: converDateToArray,
        parseDateFromArray: parseDateFromArray,
        parseDateFromString: parseDateFromString,
        isUrlForBackend: isUrlForBackend,
        isNotUrlForLogin: isNotUrlForLogin,
        buildUrlEvento: buildUrlEvento,
        Arrays: Arrays
    }
}]);
