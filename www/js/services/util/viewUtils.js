app.factory('viewUtils', function ($ionicPopup) {
    var defaultPopup = function (title, message) {
        return $ionicPopup.alert({
            title: title,
            template: message,
            buttons: [
                {
                    text: '<b>Aceptar</b>',
                    type: 'button-royal'
                }
            ]

        });
    };
    var templates = {
        loadingTemplate:
        '<div class="loader">' +
            '<svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>'+
        '</div>',
        popOver_cambiarIp: '<ion-popover-view>' +
        '   <ion-header-bar>' +
        '       <h1 class="title">Develop - Server IP</h1>' +
        '   </ion-header-bar>' +
        '   <ion-content class="padding">' +
        '       <div class="list list-inset">' +
        '           <label class="item item-input ">' +
        '                <span class="input-label">IP</span>' +
        '                <input type="text" ng-model="url">' +
        '            </label>' +
        '       </div>' +
        '           <div class="button-bar ssem-button">' +
        '               <button class="button button-small  button-stable" ng-click="cambiarServerIP(url);">Cambiar</button> ' +
        '               <button class="button button-small  button-stable" ng-click="closePopover();">Cerrar</button>' +
        '           </div>' +
        '   </ion-content>' +
        '</ion-popover-view>'


    };

    var errorPopup = function(title, message) {
        return $ionicPopup.alert({
            title: title,
            template: message,
            buttons : [
                {
                    text: '<b>Aceptar</b>',
                    type: 'button-assertive'
                }
            ]});
    };



    return {
        defaultPopup: defaultPopup,
        errorPopup: errorPopup,
        templates: templates
    }
});

