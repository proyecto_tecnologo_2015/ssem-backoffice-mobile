app.directive("ngFileModel", [function () {
    return {
        scope: {
            ngFileModel: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                if(!attributes.multiple) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.ngFileModel = {
                                lastModified: changeEvent.target.files[0].lastModified,
                                lastModifiedDate: changeEvent.target.files[0].lastModifiedDate,
                                name: changeEvent.target.files[0].name,
                                size: changeEvent.target.files[0].size,
                                type: changeEvent.target.files[0].type,
                                data: loadEvent.target.result
                            };
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                }
                else {
                    scope.ngFileModel = [];
                    angular.forEach(changeEvent.target.files, function(item){
                        var reader = new FileReader();
                        reader.onload = function(loadEvent) {
                            scope.$apply(function(){
                                scope.ngFileModel.push({
                                    lastModified: item.lastModified,
                                    lastModifiedDate: item.lastModifiedDate,
                                    name: item.name,
                                    size: item.size,
                                    type: item.type,
                                    data: loadEvent.target.result
                                });
                            });
                        };
                        reader.readAsDataURL(item);
                    });
                }
            });
        }
    }
}]);