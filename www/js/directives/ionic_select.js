/**
 * Credit: http://jsfiddle.net/cristoferdomingues/KR7KV/10/
 * 'Improved' by: Diego Estela
 * */
angular.module('ionicSelect', [])

    .directive('ionSelect', function () {
        'use strict';
        return {
            restrict: 'EAC',
            scope: {
                labelField: '@',
                placeholder: '@',
                items: '=',
                ngModel: '=?',
                ngValue: '=?'
            },
            require: '?ngModel',
            transclude: false,
            replace: false,
            templateUrl: 'js/directives/ionic_select.html',

            link: function (scope, element, attrs, ngModel) {

                scope.ngValue = scope.ngValue !== undefined ? scope.ngValue : 'item';
                scope.isObject = false;

                var isObject = function() {
                    if(scope.items && scope.items.length > 0) {
                        if(typeof scope.items[0]  === 'object') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    return false;
                };

                var getInput = function() {
                    return element.find('input')[0];
                };

                scope.selecionar = function (item) {
                    ngModel.$setViewValue(item);
                    scope.showHide = false;
                };


                element.bind('click', function () {
                    getInput().focus();
                });

                scope.openList = function () {
                    scope.ngModel = "";
                    scope.showHide = !scope.showHide;
                    return;
                };

                scope.onKeyUp = function () {
                    scope.showHide = true;
                    if (!scope.ngModel) {
                        scope.showHide = false;
                    }
                };

                scope.$watch('items', function(){
                    scope.isObject = isObject();
                });

                scope.$watch('ngModel', function (newValue) {
                    if (newValue) {
                        if(scope.isObject) {
                            element.find('input').val(newValue[scope.labelField]);
                        }
                        else {
                            element.find('input').val(newValue);
                        }
                    }
                });
            }
        };
    });