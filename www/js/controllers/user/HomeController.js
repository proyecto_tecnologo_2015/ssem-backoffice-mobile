app.controller('HomeController', function ($scope, $state, loginService, ssemMessages, navigationCache, viewUtils, $ionicLoading, eventoService, commonUtils) {

    $scope.evento = {};
    $scope.errors = [];

    $scope.toggleGroup = function() {
        $scope.mostrarDisciplinas = !$scope.mostrarDisciplinas;
    };

    $scope.$on('$ionicView.enter', function () {
        $scope.swiper = { };

        $scope.mostrarDisciplinas = false;

        var evento = navigationCache.getSelectedEvento();
        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });
        eventoService.getDatosEvento(evento.idEvento).then(
            function success(data) {
                data.fechaInicio = commonUtils.parseDateFromArray(data.fechaInicio);
                data.fechaFin = commonUtils.parseDateFromArray(data.fechaFin);
                if(angular.isArray(data.origenes)) {
                    data.origenes.forEach(function (origen) {
                        origen.tipoOrigen = origen.tipoOrigen.toLowerCase();
                    })
                }
                data.url = commonUtils.buildUrlEvento(data);
                $scope.evento = data;
                $scope.fechaDesde = $scope.evento.fechaInicio;
                $scope.fechaHasta = $scope.evento.fechaFin;
                $ionicLoading.hide();
            },
            function error(error) {
                $ionicLoading.hide();
                viewUtils.defaultPopup(ssemMessages.ERROR, error).then(function(){
                    $state.go('main');
                });
            }
        );

    });

});