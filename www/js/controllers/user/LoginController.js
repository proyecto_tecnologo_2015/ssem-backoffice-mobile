app.controller('LoginController', function($scope, $state, $rootScope,loginService, ssemMessages, navigationCache, viewUtils, $ionicLoading, $ionicPopover) {
    $scope.errors = [];
    $scope.user = {
        userName: 'vane@gmail.com',
        password: 'hola'
    };

    $scope.$on('$ionicView.enter', function(){
        $scope.url = $rootScope.SERVER_URL;
    });

    $scope.popOver = $ionicPopover.fromTemplate(viewUtils.templates.popOver_cambiarIp, {
        scope: $scope
    });

    $scope.cambiarServerIP = function(url) {
        $rootScope.initBackendUrls(url);
    };

    $scope.closePopover = function () {
        $scope.popOver.hide();
    };

    $scope.login = function() {

        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });

        loginService.doLogin($scope.user).then(
            function success(){
                navigationCache.cleanCache();
                $state.go('main').then(
                    $ionicLoading.hide()
                );
            },
            function error(error) {
                $ionicLoading.hide();
                if(!error)
                    error = ssemMessages.GENERIC_ERROR;
                viewUtils.defaultPopup(ssemMessages.ERROR, error);
            }
        );
    };
});