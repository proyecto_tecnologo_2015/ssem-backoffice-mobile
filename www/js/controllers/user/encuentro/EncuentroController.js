/**
 * Created by dalepo on 10/04/16.
 */
app.controller('EncuentroController', function($scope, encuentroService, navigationCache, commonUtils, $ionicLoading, viewUtils, ssemMessages, $state){

    $scope.goToEncuentro = function(encuentro) {
        navigationCache.setSelectedEncuentro(encuentro);
        $state.go('app.encuentros.detalleEncuentro');
    };

    $scope.$on('$ionicView.enter', function () {

        $scope.encuentros = null;

        var evento = navigationCache.getSelectedEvento();

        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });

        encuentroService.getEncuentrosEvento(evento.idEvento).then(
            function success(data) {
                $ionicLoading.hide();
                if(angular.isArray(data)){
                    if(data.length == 0) {
                        $scope.tieneEncuentros = false;
                    }
                    else {
                        $scope.encuentros = [];
                        data.forEach(function(val){
                            val.fecha = commonUtils.parseDateFromArray(val.fecha);
                            val.fechafin = commonUtils.parseDateFromArray(val.fechafin);
                            $scope.encuentros.push(val);
                        });
                        $scope.tieneEncuentros = true;
                    }
                }
            },
            function error(error) {
                $ionicLoading.hide();
                viewUtils.defaultPopup(ssemMessages.ERROR, error);
            }
        );

    });


});