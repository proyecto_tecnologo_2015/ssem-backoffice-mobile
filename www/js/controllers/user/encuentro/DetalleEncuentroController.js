/**
 * Created by dalepo on 10/04/16.
 */
app.controller('DetalleEncuentroController', function($scope, navigationCache, encuentroService, eventoService,  viewUtils, ssemMessages, commonUtils, $state, $localStorage, $filter) {

    $scope.goAltaResultado = function() {
        $localStorage.putObject('$encuentro', $scope.encuentro);
        $state.go('app.encuentros.altaResultado');
    };

    $scope.$on('$ionicView.enter', function(){

        $scope.encuentro = null;
        $scope.tienePuntajes = null;
        $scope.tieneResultados = null;
        $scope.terminoEncuentro = null;

        var evento = navigationCache.getSelectedEvento();

        eventoService.getDisciplinasEvento(evento.idEvento).then(
            function success(disciplinas) {
                $scope.disciplinas = disciplinas;
                var encuentro = navigationCache.getSelectedEncuentro();
                encuentroService.getDetalleEncuentro(encuentro.id).then(
                    function success(data) {
                        data.fecha = commonUtils.parseDateFromArray(data.fecha);
                        data.fechafin = commonUtils.parseDateFromArray(data.fechafin);
                        var disciplinasFiltradas = $filter('filter')( $scope.disciplinas, {'id' : data.idDisciplina});
                        if(disciplinasFiltradas.length > 0) {
                            data.disciplina = disciplinasFiltradas[0];
                        }
                        var now = new Date();
                        var fechaFin = new Date(data.fechafin);
                        $scope.terminoEncuentro = now.getTime() > fechaFin.getTime() ? true : false;
                        if(commonUtils.Arrays.isNotEmpty(data.resultados)) {
                            $scope.tieneResultados = true;
                            var resultado = data.resultados[0];
                            if(resultado.puntaje)
                                $scope.tienePuntajes = true;
                            else
                                $scope.tienePuntajes = false;
                        }
                        else {
                            $scope.tieneResultados = false;
                        }
                        $scope.encuentro = data;

                    },
                    function error(error) {
                        viewUtils.defaultPopup(ssemMessages.ERROR, error).then($state.go('app.encuentros'));
                    }
                )

            }
        );


    });

});