/**
 * Created by dalepo on 10/04/16.
 */
app.controller('ResultadoController', function($scope, $state,navigationCache, equipoService, viewUtils, ssemMessages, $ionicLoading, $localStorage, $ionicSideMenuDelegate, encuentroService){


    $scope.$on('$ionicView.enter', function(){

        $ionicSideMenuDelegate.canDragContent(false);

        $scope.checkPuntaje = true;
        $scope.checkPosicion = false;

        var evento = navigationCache.getSelectedEvento();
        var encuentro = $localStorage.getObject('$encuentro');

        var equipos = encuentro.equipos;
        $scope.resultados = [];

        for(var i = 0 ; i < equipos.length; i++) {
            var equipo = equipos[i];
            $scope.resultados.push({
                equipo: equipo,
                idEncuentro: encuentro.id
            });
        }

    });

    $scope.doCheckPuntaje = function () {
        $scope.checkPuntaje = !$scope.checkPuntaje;
        $scope.checkPosicion = !$scope.checkPuntaje;
    };

    $scope.doCheckPosicion = function () {
        $scope.checkPosicion = !$scope.checkPosicion;
        $scope.checkPuntaje = !$scope.checkPosicion;
    };

    $scope.altaResultados = function () {

        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });
        if($scope.checkPosicion) {
            for(var i = 0; i <$scope.resultados.length; i++) {
                $scope.resultados[i].posicion = i+1;
            }
        }
        encuentroService.altaResultado($scope.resultados).then(
            function success() {
                $ionicLoading.hide();
                viewUtils.defaultPopup(ssemMessages.SUCCESS, ssemMessages.MSG_SUCCESS_ALTA_RESULTADO).then(
                    function () {
                        $state.go('app.encuentros.detalleEncuentro');
                    },
                    function error(error){
                        viewUtils.errorPopup(ssemMessages.ERROR, error);
                    }
                )
            },
            function error(error) {
                $ionicLoading.hide();
                viewUtils.errorPopup(ssemMessages.ERROR, error);
            }
        )
    };

    $scope.onDropComplete=function(index, obj, evt){
        var otherObj = $scope.resultados[index];
        var otherIndex = $scope.resultados.indexOf(obj);
        $scope.resultados[index] = obj;
        $scope.resultados[otherIndex] = otherObj;
    };

    $scope.$on('$ionicView.leave', function(){
        $ionicSideMenuDelegate.canDragContent(true);
    });

    $scope.moveUp = function(index) {
        var newIndex = index -1;
        if(newIndex < 0)
            return;
        var swapFrom = $scope.resultados[index];
        var swapTo = $scope.resultados[newIndex];
        $scope.resultados[newIndex] = swapFrom;
        $scope.resultados[index] = swapTo;
    };
});