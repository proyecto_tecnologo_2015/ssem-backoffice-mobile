app.controller('RegistrarController', function($scope, $state,organizadorService, $ionicLoading, viewUtils, ssemMessages, commonService, commonUtils){

    $scope.$on('$ionicView.enter', function () {
        $scope.request =  {} ;
        $scope.imagen = {};
        commonService.getPaises().then(function(data){
            $scope.paises = data;
        });
    });

    $scope.confirmar = function() {
        //Tweak por el date...
        $scope.request.fechaNac = commonUtils.parseDateFromString($scope.request.fechaNac);
        if($scope.imagen.result) {
            $ionicLoading.show({
                template: viewUtils.templates.loadingTemplate
            });

            $scope.request.tipoPerfil = "ORGANIZADOR";
            $scope.request.imagen = $scope.imagen.result.data;
            delete $scope.request.telefono;
            organizadorService.registrarOrganizador({request: $scope.request}).then(
                function success(){
                    $ionicLoading.hide();
                    viewUtils.defaultPopup(ssemMessages.SUCCESS, ssemMessages.MSG_SUCCESS_REGISTRO).then(
                        function(){
                            $state.go('login');
                        }
                    );
                },
                function error(error) {
                    $ionicLoading.hide();
                    viewUtils.defaultPopup(ssemMessages.ERROR, error);
                }
            )
        }

    };

});