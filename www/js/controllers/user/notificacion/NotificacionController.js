/**
 * Created by dalepo on 04/05/16.
 */
app.controller('NotificacionController', function($scope, notificacionService, navigationCache, viewUtils, $ionicLoading, ssemMessages, $rootScope) {

    $scope.$on('$ionicView.enter', function(){
        $scope.notificacion = { };
    });

    $scope.notificar = function () {

        var evento = navigationCache.getSelectedEvento();

        if(!$scope.notificacion.title || $scope.notificacion.title.length < 1) {
            viewUtils.errorPopup(ssemMessages.ERROR, ssemMessages.VALIDATE_NOTIFICACION_TITULO);
            return;
        }
        if(!$scope.notificacion.body || $scope.notificacion.body.length < 1) {
            viewUtils.errorPopup(ssemMessages.ERROR, ssemMessages.VALIDATE_NOTIFICACION_BODY);
            return;
        }

        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });

        notificacionService.pushNotificacionEvento($scope.notificacion, evento.idEvento).then(
            function success() {
                viewUtils.defaultPopup(ssemMessages.SUCCESS, ssemMessages.MSG_SUCCESS_NOTIFICACION).then(function () {
                    $scope.notificacion = {};
                });
                $ionicLoading.hide();
            },
            function error(error) {
                $ionicLoading.hide();
                viewUtils.errorPopup(ssemMessages.ERROR, error);
            }
        );
    }

});