app.controller('EventoController', function ($scope, eventoService, $state, viewUtils, ssemMessages, $ionicLoading, commonService, commonUtils, $timeout, validationUtils, authService) {

    $scope.$on('$ionicView.enter', function () {

        $scope.evento = { };
        $scope.disciplinas = [];
        $scope.disciplina = '';
        $scope.imagenes =  {
            result: []
        };
        commonService.getPaises().then(
            function success(data) {
                $scope.paises = data;
            }
        )

    });

    $scope.addDisciplina = function(disciplina) {
        if(!commonUtils.Arrays.hasElement($scope.disciplinas, disciplina)) {
            $scope.disciplinas.push(disciplina);
        }
        var input = document.getElementById('disciplina-input');
        input.value = '';
    };

    $scope.altaEvento = function () {

        if(!validationUtils.validateLengthArray($scope.disciplinas)) {
            viewUtils.errorPopup(ssemMessages.ERROR, ssemMessages.VALIDATE_MSG_ALTA_EVENTO_DISCIPLINAS);
            return;
        }
        if(!validationUtils.validateLengthArray($scope.imagenes.result)) {
            viewUtils.errorPopup(ssemMessages.ERROR, ssemMessages.VALIDATE_MSG_ALTA_EVENTO_IMAGENES);
            return;
        }

        var listaImagenes = [];
        $scope.imagenes.result.forEach(function (val) {
            listaImagenes.push(val.data);
        });

        var user = authService.getUser();

        var eventoRequest = {
            nombre: $scope.evento.nombre,
            idOrganizador: user.id,
            fechaInicio: new Date($scope.evento.fechaDesde.getTime()),
            fechaFin: new Date($scope.evento.fechaHasta.getTime()),
            imagenes : listaImagenes,
            disciplinas: $scope.disciplinas
        };

        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });

        eventoService.crearEvento(eventoRequest).then(
            function success() {
                $ionicLoading.hide();
                viewUtils.defaultPopup(ssemMessages.SUCCESS, ssemMessages.MSG_SUCCESS_ALTA_EVENTO).then(
                    function () {
                        $state.go('main');
                    },
                    function error(error){
                        viewUtils.errorPopup(ssemMessages.ERROR, error);
                    }
                )
            },
            function error(error) {
                $ionicLoading.hide();
                if (!error || error.length == 0) {
                    error = ssemMessages.GENERIC_ERROR;
                }
                viewUtils.errorPopup(ssemMessages.ERROR, error);
            }
        )
    }
});
