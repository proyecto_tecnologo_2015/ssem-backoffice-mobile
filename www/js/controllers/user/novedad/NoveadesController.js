app.controller('NovedadesController', function($scope, novedadService, navigationCache, viewUtils, ssemMessages, commonUtils, $ionicLoading){
    $scope.$on('$ionicView.enter', function() {

        var evento = navigationCache.getSelectedEvento();
        $scope.tieneNovedades = null;
        $ionicLoading.show({
            template: viewUtils.templates.loadingTemplate
        });

        $scope.deleteNovedad = function(novedad) {
            novedadService.bajaNovedad(novedad).then(
                function success(data) {
                    if(angular.isArray(data)) {
                        $scope.novedades = [];
                        if(data.length == 0)
                            $scope.tieneNovedades = false;
                        data.forEach(function (val) {
                            val.fecha = commonUtils.parseDateFromArray(val.fecha);
                            $scope.novedades.push(val);
                        });
                    }
                    else {
                        $scope.tieneNovedades = false;
                    }
                    $ionicLoading.hide();
                },
                function error(error) {
                    $ionicLoading.hide();
                    viewUtils.defaultPopup(ssemMessages.ERROR, error);
                }
            );
        };

        novedadService.getNovedadesEvento(evento.idEvento).then(
            function success(data) {
                $scope.novedades = [];
                if(angular.isArray(data)){
                    if(data.length == 0)
                        $scope.tieneNovedades = false;
                    data.forEach(function (val) {
                        val.fecha = commonUtils.parseDateFromArray(val.fecha);
                        $scope.novedades.push(val);
                    });
                }
                else {
                    $scope.tieneNovedades = false;
                }
                $ionicLoading.hide();
            },
            function error(error) {
                $ionicLoading.hide();
            }
        )
    });

});