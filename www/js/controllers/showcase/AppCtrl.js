﻿app.controller('AppCtrl', function ($scope, $ionicModal, $ionicPopover, authService, $state, authService, commonService) {

    var user = authService.getUserId();
    commonService.getDatosOperador(user).then(
        function(data) {
            if(!data.imagen) {
                data.imagen = 'img/user.png';
            }
            $scope.user = data;
        }
    );

    $scope.logout = function() {
        authService.logout();
        $state.go('login');
    };
});