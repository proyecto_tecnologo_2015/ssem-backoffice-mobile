app.controller('MainController', function ($scope, organizadorService, viewUtils, $state, authService, ssemMessages, navigationCache) {

    $scope.eventos = [];

    $scope.$on('$ionicView.enter', function () {
        $scope.eventos = [];
        $scope.tieneEventos = null;
        getEventos();
    });
    $scope.goToEvento = function (evento) {
        delete evento.imagenes;
        navigationCache.setSelectedEvento(evento);
        $state.go('app.home');
    };

    $scope.doLogout = function() {
        authService.logout();
        $state.go('login');
    };

    var getEventos = function () {
        organizadorService.getEventosOrganizador(authService.getUserId()).then(
            function success(eventos) {
                $scope.eventos = eventos;
                if(eventos.length > 0){
                    $scope.tieneEventos = true;
                }
                else {
                    $scope.tieneEventos = false;
                }
            },
            function error(error) {
                $scope.tieneEventos = false;
                if (!error)
                    error = ssemMessages.GENERIC_ERROR;
                viewUtils.defaultPopup(ssemMessages.ERROR, error);
                $state.go('login');
            }
        );
    }
});